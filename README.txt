Archetype for a JSF/Primefaces/JPA application with some CRUD
functionality.

JSF				2.2
Primefaces: 	5.3

Start with mvn clean install tomcat7:run
The app should appear under /jsfjpa on port 8080