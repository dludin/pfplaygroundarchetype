package it.pkg.beans;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import it.pkg.entity.User;
import it.pkg.entity.UserRole;
import it.pkg.persistence.DataRepository;


/**
 *
 * @author r0y
 *
 */
@SuppressWarnings("serial")
@SessionScoped
@Named("userBean")
public class UserBean implements Serializable {

	@Inject
	@DataRepository
	private EntityManager em;

	private String user;
	private String password;

	private FacesContext context;
	private ExternalContext externalContext;
	private HttpServletRequest request;
	private HttpSession session;

	public UserBean() {
		super();
	}

	@PostConstruct
	public void init() {
		context = FacesContext.getCurrentInstance();
		externalContext = context.getExternalContext();
		request = (HttpServletRequest) externalContext.getRequest();
		session = request.getSession(false);

	}

	public void login() {
		context = FacesContext.getCurrentInstance();
		externalContext = context.getExternalContext();
		request = (HttpServletRequest) externalContext.getRequest();
		System.out.println("sdfsa");
		try {
			int idUser = (int) em
					.createQuery(
							"SELECT u.id FROM User u WHERE username = :username")
					.setParameter("username", user).getSingleResult();
			request.login("" + idUser, password);
			user = find(user, password, idUser);
			externalContext.getSessionMap().put("user", user);
			externalContext.redirect("index.html");
			System.out.println("Fertig");
		} catch (Exception e) {
			e.printStackTrace();
			context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Access denied", null));
		}

	}

	public String find(String user, String pass, int id) {

		User u = em.find(User.class, id);

		if (user.equals(u.getUsername()) && pass.equals(u.getPassword())) {
			return user;
		}
		return "not authorized";
	}

	public void logout() {

		// Destroys the session for this user.
		if (session != null)
			session.invalidate();

		// Redirects back to the initial page.
		try {
			externalContext.redirect("./");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = getHashMd5(password);
	}

	public String getHashMd5(String clearPass) {
		MessageDigest m;
		try {
			m = MessageDigest.getInstance("MD5");
			m.update(clearPass.getBytes(), 0, clearPass.length());
			return new BigInteger(1, m.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	public boolean isAdmin() {
		List<UserRole> listUserRole = em
				.createQuery(
						"FROM UserRole ur WHERE ur.user.username = :userName AND ur.role.id = 1")
				.setParameter("userName", user).getResultList();
		if (listUserRole.isEmpty()) {
			return false;
		}
		return true;
	}
}
