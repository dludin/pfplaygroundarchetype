/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.pkg.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import it.pkg.entity.Role;
import it.pkg.entity.User;
import it.pkg.entity.UserRole;
import it.pkg.persistence.DataRepository;


/**
 *
 * @author kenny
 */
@Named("roleController")
@SessionScoped
public class RoleController extends AbstractController<Role> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Inject
	@DataRepository
	private EntityManager em;


	public RoleController(){
		super(Role.class);
	}
	
	public void deleteRole() {
		EntityTransaction tx = em.getTransaction();
		@SuppressWarnings("unchecked")
		List<UserRole> lustUserRole = em
				.createQuery("FROM UserRole ur WHERE ur.role.id =:idRole")
				.setParameter("idRole", getSelectedRecord().getId()).getResultList();
		for (UserRole ur : lustUserRole) {
			tx.begin();
			em.remove(ur);
			tx.commit();
		}
		tx.begin();
		em.remove(getSelectedRecord());
		tx.commit();

	}

	
	//@TestAnnotation
	public void test() {
		printFacesMessage("Hello", "this is a test by Dani");
	}
}