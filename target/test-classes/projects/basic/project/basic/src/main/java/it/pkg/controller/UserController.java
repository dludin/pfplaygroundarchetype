/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.pkg.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import it.pkg.beans.UserBean;
import it.pkg.entity.Role;
import it.pkg.entity.User;
import it.pkg.entity.UserRole;
import it.pkg.persistence.DataRepository;


/**
 *
 * @author kenny
 */
@SuppressWarnings("serial")
@Named("userController")
@SessionScoped
public class UserController implements Serializable {

	@Inject
	@DataRepository
	private EntityManager em;

	private String controlPassword;
	private String password;

	@Inject
	UserBean userbean;

	private User selectedUser;
	private boolean isAdmin = false;

	@PostConstruct
	void init() {

		System.out.println("init User");
		selectedUser = new User();
		password = "";
		controlPassword = "1";
		isAdmin = false;
	}

	@SuppressWarnings("unchecked")
	public void updateUser() {
		
		if (password.equals(controlPassword)) {
			selectedUser.setPassword(password);
			selectedUser.setPassword(userbean.getHashMd5(selectedUser
					.getPassword()));
		} 
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		em.merge(selectedUser);
		tx.commit();

		if (isAdmin) {
			UserRole userrole = new UserRole();
			userrole.setUser(selectedUser);
			userrole.setRole(em.find(Role.class, 1));
			tx.begin();
			em.merge(userrole);
			tx.commit();
		} else if (selectedUser.getId() != 0) {
			List<UserRole> listUserRole = em
					.createQuery(
							"FROM UserRole ur WHERE ur.user.id = :idUser AND ur.role.id = 1")
					.setParameter("idUser", selectedUser.getId())
					.getResultList();
			for (UserRole ur : listUserRole) {
				tx.begin();
				em.remove(ur);
				tx.commit();
			}
		}
		selectedUser = new User();
		password = "";
		controlPassword = "1";
		isAdmin = false;
	}

	public void deleteUser() {
		EntityTransaction tx = em.getTransaction();
		@SuppressWarnings("unchecked")
		List<UserRole> lustUserRole = em
				.createQuery("FROM UserRole ur WHERE ur.user.id =:idUser")
				.setParameter("idUser", selectedUser.getId()).getResultList();
		for (UserRole ur : lustUserRole) {
			tx.begin();
			em.remove(ur);
			tx.commit();
		}
		tx.begin();
		em.remove(selectedUser);
		tx.commit();

		selectedUser = new User();
		password = "";
		controlPassword = "1";
		isAdmin = false;
	}

	public void addUser() {
		password = "";
		controlPassword = "";
		isAdmin = false;
		selectedUser = new User();
	}

	public void cancel() {
		selectedUser = new User();
		controlPassword = "";
		password = "1";
	}

	public void updatePassword() {
		if (password == controlPassword) {
			selectedUser.setPassword(password);
		}
	}

	// GETTER & SETTER

	public List<User> getUserlist() {
		return em.createQuery("FROM User u").getResultList();
	}

	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}

	public boolean getAdmin() {
		if (selectedUser != null) {
			List<UserRole> listUserRole = em
					.createQuery(
							"FROM UserRole ur WHERE ur.user.id = :idUser AND ur.role.id = 1")
					.setParameter("idUser", selectedUser.getId())
					.getResultList();

			if (!listUserRole.isEmpty()) {
				return true;
			}
		}
		return false;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getControlPassword() {
		return controlPassword;
	}

	public void setControlPassword(String controlPassword) {
		this.controlPassword = controlPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
