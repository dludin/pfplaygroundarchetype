package it.pkg.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
 
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

import it.pkg.entity.Employee;
 
 
@SuppressWarnings("serial")
@SessionScoped
@Named("employeeBean")

public class EmployeeBean implements Serializable{
    private List<Employee> employees = new ArrayList<Employee>();
 
    public EmployeeBean(){
 
    }
 
    @PostConstruct
    public void populateEmployeeList(){
        for(int i = 1 ; i <= 10 ; i++){
            Employee emp = new Employee();
            emp.setEmployeeId(String.valueOf(i));
            emp.setEmployeeName("Employee #"+i);
            this.employees.add(emp);
        }
    }
 
    public List<Employee> getEmployees() {
        return employees;
    }
 
    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
    public String getTitle(){
    	return "A simple pf datatable example";
    }
}