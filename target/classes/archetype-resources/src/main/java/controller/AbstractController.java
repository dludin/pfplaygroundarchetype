#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.el.ELContext;
import javax.enterprise.context.SessionScoped;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpServletRequest;

import ${package}.persistence.DataRepository;


@SessionScoped
public abstract class AbstractController<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private T selectedRecord;
	private List<T> recordList;
	private Class<T> clazz;
	private List<T> filteredRecordList = new ArrayList<>();

	@Inject
	@DataRepository
	private EntityManager em;

	public AbstractController() {
	}

	public AbstractController(Class<T> clazz) {
		this.clazz = clazz;
		try {
			selectedRecord = clazz.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public void save() {
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			em.persist(selectedRecord);
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
		}
		clear();
		doHardReload();
		printFacesMessage("Saved", "");
	}

	public void delete(Object o) {
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			em.remove(o);
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
		}
		clear();
		doHardReload();
		printFacesMessage("DELETED", "");
	}

	protected void printFacesMessage(String summary, String detail) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(summary, detail));
	}

	public void clear() {
		try {
			em.clear();
			selectedRecord = clazz.newInstance();
			loadRecordList();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public void doHardReload() {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		try {
			ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadRecordList() {
		recordList = em.createQuery("FROM " + clazz.getSimpleName()).getResultList();
		filteredRecordList = recordList;
	}

	public List<T> getRecordList() {
		loadRecordList();
		return recordList;
	}

	public void setRecordList(List<T> recordList) {
		this.recordList = recordList;
	}

	public List<T> getFilteredRecordList() {
		if (filteredRecordList == null) {
			loadRecordList();
		}
		return filteredRecordList;
	}

	public void setFilteredRecordList(List<T> filteredRecordList) {
		this.filteredRecordList = filteredRecordList;
	}

	public T getSelectedRecord() {
		return selectedRecord;
	}

	public void setSelectedRecord(T selectedRecord) {
		this.selectedRecord = selectedRecord;
	}
}