#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
CREATE DATABASE ${artifactId} CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE USER 'test'@'localhost' IDENTIFIED BY 'test';
GRANT ALL ON ${artifactId}.* TO 'test'@'localhost';
GRANT ALL PRIVILEGES ON ${artifactId}.* TO 'test'@'localhost' IDENTIFIED BY 'test' WITH GRANT OPTION;

INSERT INTO pfplaygroundarchetype.roles VALUES (1, "keine Bemerkung", "admin");
INSERT INTO pfplaygroundarchetype.roles VALUES (1, "keine Bemerkung", "user");
INSERT INTO pfplaygroundarchetype.jusers VALUES (1, "admin", "admin");
INSERT INTO pfplaygroundarchetype.jusers VALUES (2, "test", "test");
INSERT INTO pfplaygroundarchetype.users_roles  VALUES (1, 1, 1);
INSERT INTO pfplaygroundarchetype.users_roles  VALUES (2, 2, 2);


select j.id_juser, j.username, r.role from jusers j, roles r, users_roles ur
where ur.id_role = j.id_juser
and ur.id_juser = r.id_role
order by j.username;

select jusers.id_juser, jusers.username, roles.role from jusers
left outer join users_roles on users_roles.id_role = jusers.id_juser
left outer join roles on  users_roles.id_juser = roles.id_role
order by jusers.id_juser;
