#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.beans;

import java.io.Serializable;
import java.util.Date;

import javax.faces.bean.SessionScoped;

import javax.inject.Named;

@SuppressWarnings("serial")
@SessionScoped
@Named("testBean")

public class TestBean implements Serializable {
	public Date getCurrentDate(){
		return new Date();
	}
    public String getTitle(){
    	return "Simple view with a simple bean";
    }

}
